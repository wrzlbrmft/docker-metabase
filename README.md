# docker-metabase

Business Intelligence, Dashboards, and Data Visualization in Docker.

Based on https://hub.docker.com/r/metabase/metabase .

```
cd docker/compose
```

Run either `./up-with-mariadb.sh` or `./up-with-postgres.sh`.

Next, open http://localhost:3000/ .

See also:

  * https://www.metabase.com/
  * https://hub.docker.com/r/wrzlbrmft/metabase/tags

## Kubernetes (GKE)

```
helm install --set host=<host> metabase docker/helm/metabase
```

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
