#!/usr/bin/env sh
. metabase/.env

DOCKER_COMPOSE_OPTS="-f metabase/docker-compose.yml"

docker compose ${DOCKER_COMPOSE_OPTS} down
