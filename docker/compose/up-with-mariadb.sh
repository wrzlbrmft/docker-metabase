#!/usr/bin/env sh
. metabase/.env

DOCKER_COMPOSE_OPTS="-f metabase/docker-compose.yml"

SERVICES="\
    metabase-mariadb \
    mariadb \
"

[ "$1" != "--no-pull" ] && \
    docker compose ${DOCKER_COMPOSE_OPTS} pull --include-deps ${SERVICES}

docker compose ${DOCKER_COMPOSE_OPTS} up ${SERVICES}
